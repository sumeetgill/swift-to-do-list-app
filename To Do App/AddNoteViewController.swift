//
//  AddNoteViewController.swift
//  To Do App
//
//  Created by Sumeet Gill on 2014-08-17.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

import UIKit

class AddNoteViewController: UIViewController {

    @IBOutlet var noteTitleTextField: UITextField? = UITextField()
    @IBOutlet var noteDetailsTextView: UITextView? = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        var noteList:NSMutableArray? = userDefaults.objectForKey("noteList") as? NSMutableArray
        
        var dataSet:NSMutableDictionary = NSMutableDictionary()
        dataSet.setObject(noteTitleTextField?.text, forKey: "noteTitle")
        dataSet.setObject(noteDetailsTextView?.text, forKey: "noteDetails")
        
        
        if(noteList?.count > 0){ // check to see if data is already available
            var newMutableList:NSMutableArray = NSMutableArray()
            
            for dict:AnyObject in noteList! {
                newMutableList.addObject(dict as NSDictionary)
            }
            
            userDefaults.removeObjectForKey("noteList")
            newMutableList.addObject(dataSet)
            userDefaults.setObject(newMutableList, forKey: "noteList")
            
        } else { // This is the first todo item in the list
            userDefaults.removeObjectForKey("noteList")
            
            noteList = NSMutableArray()
            noteList?.addObject(dataSet)
            
            userDefaults.setObject(noteList, forKey:"noteList")
            
        }
        
        userDefaults.synchronize()
        
        self.navigationController.popToRootViewControllerAnimated(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
