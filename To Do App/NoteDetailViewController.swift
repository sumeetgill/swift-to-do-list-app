//
//  NoteDetailViewController.swift
//  To Do App
//
//  Created by Sumeet Gill on 2014-08-17.
//  Copyright (c) 2014 Sumeet Gill. All rights reserved.
//

import UIKit

class NoteDetailViewController: UIViewController {

    @IBOutlet var noteTitleTextField: UITextField? = UITextField()
    @IBOutlet var noteDetailsTextView: UITextView? = UITextView()

    var toDoData:NSDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noteTitleTextField?.userInteractionEnabled = false
        noteDetailsTextView?.userInteractionEnabled = false
        
        noteTitleTextField?.text = toDoData.objectForKey("noteTitle") as String
        noteDetailsTextView?.text = toDoData.objectForKey("noteDetails") as String
    }
    
    @IBAction func deleteNotePressed(sender: AnyObject) {
        
        var userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var noteListArray:NSMutableArray = userDefaults.objectForKey("noteList") as NSMutableArray
        
        var mutableNoteList:NSMutableArray = NSMutableArray()
        for dict:AnyObject in noteListArray {
            mutableNoteList.addObject(dict as NSDictionary)
        }
        
        mutableNoteList.removeObject(toDoData)
        
        userDefaults.removeObjectForKey("noteList")
        userDefaults.setObject(mutableNoteList, forKey: "noteList")
        userDefaults.synchronize()
        
        self.navigationController.popToRootViewControllerAnimated(true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }


}
